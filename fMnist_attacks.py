import torch
import torch.nn.functional as F
from torch.utils.data.sampler import SubsetRandomSampler
from support import Network, test
import torchvision.transforms as transforms
from torchvision import datasets
import numpy as np
import matplotlib.pyplot as plt
from secml.adv.attacks import CAttackPoisoningSVM

torch.set_grad_enabled(True)
transform = transforms.ToTensor()
test_set = datasets.FashionMNIST(
    root='./data/FashionMNIST'
    ,train=False
    ,download=True
    ,transform = transform)
train_sampler = SubsetRandomSampler(list(range(48000)))
valid_sampler = SubsetRandomSampler(list(range(12000)))

model = Network()
#PATH = './model1.pth'
#model = torch.load(PATH)
model.eval()
test_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=True)
epsilons = [0, .05, .1, .15, .2, .25, .3]
adv_examples = []
accuracies = []
examples = []

for eps in epsilons:
    acc, ex = test(model, test_loader, eps)
    accuracies.append(acc)
    examples.append(ex)

plt.figure(figsize=(5,5))
plt.plot(epsilons, accuracies, "*-")
plt.yticks(np.arange(0, 1.1, step=0.1))
plt.xticks(np.arange(0, .35, step=0.05))
plt.title("Accuracy vs Epsilon")
plt.xlabel("Epsilon")
plt.ylabel("Accuracy")
plt.show()

