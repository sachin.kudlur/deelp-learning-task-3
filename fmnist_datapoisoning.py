import torch, torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader, random_split
from torch.utils.data.dataloader import default_collate
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator
import numpy as np

preProcessImageData = transforms.Compose([transforms.ToTensor()])
mnist_dataset = torchvision.datasets.FashionMNIST('./train/', train=True, transform=preProcessImageData, download=True)
valid_set, test_set, train_set = random_split(mnist_dataset, [10000,10000,40000])
assert(len(train_set)+len(valid_set)+len(test_set)==len(mnist_dataset))
plt.hist([float(label) for _,label in train_set], alpha=0.7, ec='white', bins=10, range=(-0.5,9.5))
plt.title('Class Distribution in Fashion MNIST')
plt.xlabel('Class')
plt.gca().xaxis.set_major_locator(FixedLocator(locs=[*range(10)]))
plt.show()
train_loader = DataLoader(train_set,
                          batch_size=16,
                          shuffle=True)

class ConvolutionalNeuralNet(nn.Module) :
    def __init__(self) :
        super(ConvolutionalNeuralNet, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=1,           # 1 colour channel input
                               out_channels=8,          # 8 channel output
                               kernel_size=(5,5),       # 5x5 filter
                               stride=1,                # 1 stride
                               padding=2,               # 1 padding
                               dilation=1)              # no dilation (kernel is solid)

        self.conv2 = nn.Conv2d(in_channels=8,           # 8 channel input
                               out_channels=1,          # 1 channel output
                               kernel_size=(5,5),       # 5x5 filter
                               stride=1,                # 1 stride
                               padding=2,               # 1 padding
                               dilation=1)              # no dilation (kernel is solid)

        self.linear1 = nn.Linear(in_features=28*28*1,
                                 out_features=10)

    def forward(self,X) :
        X_res = X
        X = self.conv1(X)         # apply CNN layer
        X = torch.sigmoid(X)      # activiation function
        X = self.conv2(X)         # apply CNN layer
        X = torch.sigmoid(X)      # activiation function
        X = X + X_res             # apply residual
        X = X.view(-1,28*28*1)    # reshape the tensor for input
        X = self.linear1(X)       # apply linear layer
        X = torch.sigmoid(X)      # activation function
        return X

def Train(model,lossFunction,optimiser,
          trainingDataset,validationDataset,
          maxEpochs=1000,batchSize=0,device="",
          verbose=True, verboseFrequency=100,
          collate_fn=default_collate) :

    #device defaults to gpu if avaliable else cpu
    if device=="":
        device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model=model.to(device)

    #batchsize defaults to full batch
    if batchSize==0 :
        trainingDataLoader = [collate_fn(trainingDataset)]
        validationDataLoader = [collate_fn(validationDataset)]
    else :
        #define data loaders
        trainingDataLoader = DataLoader(trainingDataset,
                                        batch_size=batchSize,
                                        shuffle=True,
                                        collate_fn=collate_fn)

        validationDataLoader = DataLoader(validationDataset,
                                          batch_size=batchSize,
                                          collate_fn=collate_fn)

    # initialise objects for training statistics
    trainingLogLoss=[]
    trainingAccuracy=[]
    validationLogLoss=[]
    validationAccuracy=[]

    # loop through epochs
    if verbose:
        print("{0:6}|{1:8}|{2:8}|{3}".format("Epoch","Train","Valid","Progress"))

    for epoch in range(maxEpochs+1) :
        epochLogLoss = 0
        epochAccuracy = 0
        for i, (images_train,labels_train) in enumerate(trainingDataLoader) :
            #send training data to cpu or gpu
            images_train=images_train.to(device)
            labels_train=labels_train.to(device)

            #clear gradient
            optimiser.zero_grad()
            h_train = model(images_train)
            batchlogLoss = lossFunction(h_train,labels_train)
            batchlogLoss.backward()
            optimiser.step()

            _,predict_train=h_train.max(1)
            epochAccuracy+=(predict_train==labels_train).sum()/(1.*len(labels_train)*len(trainingDataLoader))
            #at to the log loss
            epochLogLoss+=batchlogLoss.item()/len(trainingDataLoader)

        #store training statistics
        trainingLogLoss.append(epochLogLoss)
        trainingAccuracy.append(epochAccuracy)

        #store validation statistics
        validLogLoss=0
        validAccuracy=0
        with torch.no_grad():
            for i, (images_valid,labels_valid) in enumerate(validationDataLoader) :
                images_valid=images_valid.to(device)
                labels_valid=labels_valid.to(device)

                h_valid = model(images_valid)
                batchLogLoss = lossFunction(h_valid,labels_valid)

                _,predict_valid=h_valid.max(1)

                validAccuracy+=(predict_valid==labels_valid).sum()/(1.*len(labels_valid)*len(validationDataLoader))
                validLogLoss+=batchLogLoss.item()/len(validationDataLoader)

            validationLogLoss.append(validLogLoss)
            validationAccuracy.append(validAccuracy)

        if ((epoch-1) % verboseFrequency== 0):
            print(" ")
        

    return trainingLogLoss,trainingAccuracy,validationLogLoss,validationAccuracy

def poison_image(image):
    """apply watermark to image"""
    image[0,-1,-1]=1.
    image[0,-1,-3]=1.
    image[0,-3,-1]=1.
    image[0,-3,-3]=1.
    image[0,-2,-2]=1.
    return image

class PoisonedDataset(Dataset):

    def __init__(self, original_dataset, poison_probability=0.01, poison_label=3):
        # apply poison
        self.dataset = [(poison_image(image), poison_label) if np.random.rand()<poison_probability else (image,label)
                            for image,label in original_dataset]

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        return self.dataset[idx]

poison_probability=0.01
poison_label=3

# generate poisoned dataset
poisoned_train_set = PoisonedDataset(train_set, poison_probability, poison_label)
plt.hist([float(label) for _,label in poisoned_train_set], alpha=0.7, ec='white', bins=10, range=(-0.5,9.5))
plt.title('Class Distribution in Poisoned Fashion MNIST')
plt.xlabel('Class')
plt.gca().xaxis.set_major_locator(FixedLocator(locs=[*range(10)]))
plt.show()

poison_train_loader = DataLoader(poisoned_train_set,
                                 batch_size=16,
                                 shuffle=True)
poisoned_convolutional_neural_net = ConvolutionalNeuralNet()

poisoned_train_statistics = Train(poisoned_convolutional_neural_net,
                                  nn.CrossEntropyLoss(),
                                  optim.Adam(poisoned_convolutional_neural_net.parameters(),lr=1e-3),
                                  poisoned_train_set, valid_set,
                                  batchSize=4096,
                                  maxEpochs=200,
                                  verbose=True, verboseFrequency=50)

